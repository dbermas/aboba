

#ifndef IMAGE_ROTATION_BMPREADER_H
#define IMAGE_ROTATION_BMPREADER_H

#include <stdio.h>
#include "../include/bmp.h"

FILE* openBMP(char* fileName);

#endif //IMAGE_ROTATION_BMPREADER_H
