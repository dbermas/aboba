#ifndef IMAGE_ROTATION_DESERIALIZER_H
#define IMAGE_ROTATION_DESERIALIZER_H

#include <stdio.h>
#include "../include/image.h"


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status fromBMP( FILE* in, struct image* img );
#endif //IMAGE_ROTATION_DESERIALIZER_H
