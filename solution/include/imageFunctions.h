

#ifndef IMAGE_ROTATION_IMAGEFUNCTIONS_H
#define IMAGE_ROTATION_IMAGEFUNCTIONS_H

#include "../include/image.h"

struct image rotate(struct image,int angle);


#endif //IMAGE_ROTATION_IMAGEFUNCTIONS_H
