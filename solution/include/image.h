
#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include "../include/pixel.h"


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image createImage(uint64_t width, uint64_t height);

#endif //IMAGE_ROTATION_IMAGE_H
