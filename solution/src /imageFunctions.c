#include <stdlib.h>
#include "../include/image.h"


struct image transposition(struct image imageToRotateOnce){
    struct image tempImage;
    tempImage = createImage(imageToRotateOnce.height,imageToRotateOnce.width);
    for (uint64_t i = 0; i <imageToRotateOnce.height ; i++) {
        for(uint64_t j = 0;j < imageToRotateOnce.width;j++){
            tempImage.data[(tempImage.height-j-1)*tempImage.width+i] = imageToRotateOnce.data[i*imageToRotateOnce.width+j];
        }

    }
    return  tempImage;
}

struct image rotate(struct image imageToRotate, int angle){
    int rotationCount;
    if (angle<0){
        rotationCount = 4-abs(angle)/90;
    }
    else{
        rotationCount  = abs(angle)/90;
    }
    for (int i = 0; i < rotationCount; i++) {
    imageToRotate = transposition(imageToRotate);
    }
    return imageToRotate;
}
