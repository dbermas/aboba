
#include <stdio.h>
#include <stdlib.h>




FILE *openBMP(char *fileName) {
    FILE *fp = fopen(fileName, "rb");
    if (!fp) {
        fprintf(stderr,"Can't load file \'%s'\n", fileName);
        exit(0);
    }

    return fp;
}


