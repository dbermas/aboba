
#include <stdio.h>
#include <stdlib.h>



FILE *writeBMP(char *fileName) {
    FILE *fp = fopen(fileName, "wb");
    if (!fp) {
        fprintf(stderr,"Can't load file \'%s'\n", fileName);
        exit(0);
    }

    return fp;
}

