#include <stdio.h>
#include "../include/serializer.h"
#include "../include/bmp.h"

#define BMP_TYPE 0x4D42
#define BMP_BIT_COUNT 24
#define BMP_RESERVED 0
#define BMP_PLANES 1
#define BMP_SIZE 40
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0
#define BYTES_PER_PIXEL 3



enum write_status toBMP(FILE *out, struct image const* img) {
    uint64_t width = img->width;
    uint64_t height = img->height;
    uint32_t offset = sizeof(struct bmpHeader);

    const uint8_t padding = countPadding(width * sizeof(struct pixel));
    const uint32_t imageSize = width * height * BYTES_PER_PIXEL;
    const uint32_t fileSize = offset + imageSize + height * padding;

    struct bmpHeader header = {
            .bfType = BMP_TYPE,
            .bfileSize = fileSize,
            .bfReserved = BMP_RESERVED,
            .bOffBits = offset,
            .biSize = BMP_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BIT_COUNT,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = imageSize,
            .biXPelsPerMeter = BMP_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_Y_PELS_PER_METER,
            .biClrUsed = BMP_CLR_USED,
            .biClrImportant = BMP_CLR_IMPORTANT};

    fwrite(&header,sizeof (struct bmpHeader),1,out);
    if(ferror(out)){
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(&img->data[i * img->width], BYTES_PER_PIXEL, img->width, out) != img->width)
            return WRITE_ERROR;
        for (uint8_t j = 0; j < padding; j++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

