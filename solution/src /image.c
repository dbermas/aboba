#include <stdlib.h>
#include "../include/image.h"


struct image createImage(uint64_t width, uint64_t height) {
    struct image newImage;

    newImage.width = width;
    newImage.height = height;
    newImage.data = (struct pixel*)malloc(sizeof(struct pixel) * width * height);
    return newImage;
}
