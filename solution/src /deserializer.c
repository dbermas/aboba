

#include <malloc.h>
#include "../include/bmp.h"
#include "../include/deserializer.h"

#define BMP_TYPE 0x4D42
#define BMP_BIT_COUNT 24






enum read_status fromBMP(FILE* in, struct image* img ){
    struct bmpHeader bmpHeader;
    fread(&bmpHeader,sizeof(struct bmpHeader),1,in);
    if(ferror(in)){
        return READ_ERROR;
    }

    if (bmpHeader.bfType != BMP_TYPE){
        return READ_INVALID_SIGNATURE;
    }

    if (bmpHeader.biBitCount != BMP_BIT_COUNT){
        return READ_INVALID_BITS;
    }

    img->height = bmpHeader.biHeight;
    img->width = bmpHeader.biWidth;

    img->data = (struct pixel*)malloc(sizeof(struct pixel) * bmpHeader.biWidth * bmpHeader.biHeight);
    uint8_t padding = countPadding(img->width*sizeof(struct pixel));
    for (uint64_t i = 0; i < img->height; i++) {
        fread(&img->data[i*img->width],sizeof(struct pixel),img->width,in );
        fseek(in,padding,SEEK_CUR);
    }

    return READ_OK;
}

