#include <stdio.h>

#include <stdlib.h>
#include "../include/bmpReader.h"
#include "../include/bmpWriter.h"
#include "../include/deserializer.h"
#include "../include/serializer.h"
#include "../include/imageFunctions.h"
#include "../include/validateAngle.h"

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr,"Syntax: bmpreader <bmp file> <destination file> <angle>\n");
        return 0;
    };
    int angle = atoi(argv[3]);
    if(!validateValue(angle)){
        fprintf(stderr,"Error: permitted values: 0, 90, 180, 270, -90, -180, 270");
    }

    char *fileName = argv[1];

    FILE *in = openBMP(fileName);

    struct image openImage;
    enum read_status openImageStatus = fromBMP(in, &openImage);
    if (openImageStatus != READ_OK){
        exit(1);
    }
    fclose(in);

    openImage = rotate(openImage, angle);

    char *outputFileName = argv[2];
    FILE *out = writeBMP(outputFileName);

    enum write_status writeImageStatus = toBMP(out,&openImage);
    if (writeImageStatus != WRITE_OK){
        exit(1);

    }
    fclose(out);
    return 0;
}
